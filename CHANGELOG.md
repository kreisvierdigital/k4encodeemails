#  k4encodeEmails Changelog


## 2.3.1 - 2022.04.19
### Fixed
- Twig 3 support for Craft 4

## 2.2.4 - 2021.08.07
### Fixed
- repository name, versioning

## 2.2.1 - 2021.02.16
### Fixed
- i18n handle fixed, set correct author

## 2.2.0 - 2021.02.15
### Changed
- Plugin is now based on StandalonePHPEnkoder by (originally) Hivelogic

## 2.1.1 - 2020.11.25
### Fixed
- Comply to PSR-4 standard

## 2.1.0 - 2018.07.06
### Fixed
- namespace issues
- updated plugin handle to new format

## 2.0.0 - 2017.03.16
### Added
- Initial release
