#  k4encodeEmails plugin for Craft CMS 3.x

Plugin that allows you to encode emailaddresses in your CMS content.

![Screenshot](resources/img/plugin-logo.png)

## Installation

To install  k4encodeEmails, follow these steps:

1. Navigate to your projects root folder and install the plugin with composer: composer require k4/k4encodeemails
2.  -OR- do a `git clone https://bitbucket.org/kreisvierdigital/k4encodeemails/k4encodeemails.git` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require https://bitbucket.org/kreisvierdigital/k4encodeemails/k4encodeemails`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `k4encodeemails` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

 k4encodeEmails requires Craft 3 Release Candidate 1 (RC1) or later to work correctly.

## k4encodeEmails Overview

Plugin that allows you to encode email addresses in your rendered template content so they are less likely to be harvested by bots.


## Using k4encodeEmails
Add to your template:

	<p>{{ entry.body |encodeEmails|raw }}</p>

In this example, all emailaddresses in entry.body are encoded, both in text and mailto-links.


Brought to you by [Stefan Friedrich](http://www.kreisvier.ch/)