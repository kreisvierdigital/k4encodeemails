<?php
/**
 * Email Obfuscator plugin for Craft CMS 3.x
 *
 * Adds some Twig filters to obfuscate emails in field content. Uses Standalone PHPEnkoder by Jonathan Nicol
 *
 * @link      https://www.kreisvier.ch/
 * @copyright Copyright (c) 2019-22 kreisvier communications, Mischa Sprecher
 */


namespace k4\k4encodeemails;

use k4\k4encodeemails\twigextensions\EncodeEmailsTwigExtension;

use Craft;
use craft\base\Plugin;
use craft\events\PluginEvent;
use yii\base\Event;


class K4encodeemails extends Plugin
{

    /**
     * @var k4encodeemails
     */
    public static $plugin;


    public function init()
    {
        parent::init();
        self::$plugin = $this;
        
        Craft::$app->view->registerTwigExtension(new EncodeEmailsTwigExtension());
        
    }

}
