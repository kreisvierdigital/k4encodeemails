<?php
/**
 * Email Obfuscator plugin for Craft CMS 3.x
 *
 * Adds some Twig filters to obfuscate emails in field content. Uses Standalone PHPEnkoder by Jonathan Nicol
 *
 * @link      https://www.kreisvier.ch/
 * @copyright Copyright (c) 2020 kreisvier communications, Mischa Sprecher
 */

/**
 *  k4encodeEmails config.php
 *
 * Completely optional configuration settings for  k4encodeEmailsCraft3 if you want to customize some
 * of its more esoteric behavior, or just want specific control over things.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'k4encodeemailscraft3.php' and make
 * your changes there.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as well, so you can
 * have different settings groups for each environment, just as you do for 'general.php'
 */

return [

    // This controls the universe
    "k4rocks" => true,

];
