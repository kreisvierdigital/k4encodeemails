<?php
/**
 * Email Obfuscator plugin for Craft CMS 3.x
 *
 * Adds some Twig filters to obfuscate emails in field content. Uses Standalone PHPEnkoder by Jonathan Nicol
 *
 * @link      https://www.kreisvier.ch/
 * @copyright Copyright (c) 2020 kreisvier communications, Mischa Sprecher
 */

return [
    'plugin-loaded'     => 'Das Plugin wurde erfolgreich geladen.',
    'install-success'   =>  'Das Plugin wurde erfolgreich installiert. Sie können jetzt den Tag {{ block.textfield | encodeEmails | raw }} in Ihren Templates verwenden.',
];