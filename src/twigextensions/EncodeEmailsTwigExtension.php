<?php 
/**
 * Email Obfuscator plugin for Craft CMS 3.x
 *
 * Adds some Twig filters to obfuscate emails in field content. Uses Standalone PHPEnkoder by Jonathan Nicol
 *
 * @link      https://www.kreisvier.ch/
 * @copyright Copyright (c) 2019-22 kreisvier communications, Mischa Sprecher
 */

namespace k4\k4encodeemails\twigextensions;

use k4\k4encodeemails\K4EncodeEmails;

use Craft;
use Craft\web\twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Markup;
use StandalonePHPEnkoder;


class EncodeEmailsTwigExtension extends AbstractExtension
{

    protected $enkoder;
    
    public function __construct()
    {
        $this->enkoder = new StandalonePHPEnkoder();
    }

    public function getName()
        {
            return 'k4 encode emails';
        }

    public function getFilters()
    {
        // https://twig.symfony.com/doc/2.x/advanced.html
        $needs_env = ['needs_environment' => true];
        
        return [
            new TwigFilter('encode', [$this, 'encodeFilter'], $needs_env),
            new TwigFilter('encodeEmails', [$this, 'encodeEmailsFilter'], $needs_env),
            new TwigFilter('encodeMailtos', [$this, 'encodeMailtosFilter'], $needs_env),
            new TwigFilter('encodePlaintext', [$this, 'encodePlaintextFilter'], $needs_env),
        ];
    }

    public function getTwigMarkup(Environment $env, $str)
    {
        $charset = $env->getCharset();
        return new Markup($str, $charset);
    }
    
    public function encodeFilter(Environment $env, $str, $message = 'JavaScript is required to reveal this message.')
    {
        $str = $this->enkoder->enkode($str, $message);
        return $this->getTwigMarkup($env, $str);
    }
    
    public function encodeEmailsFilter(Environment $env, $str)
    {
        $str = $this->enkoder->enkodeAllEmails($str);
        return $this->getTwigMarkup($env, $str);
    }
    
    public function encodeMailtosFilter(Environment $env, $str)
    {
        $str = $this->enkoder->enkodeMailtos($str);
        return $this->getTwigMarkup($env, $str);
    }
    
    public function encodePlaintextFilter(Environment $env, $str)
    {
        $str = $this->enkoder->enkodePlaintextEmails($str);
        return $this->getTwigMarkup($env, $str);
    }

}



